<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\email
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\email\EmailException;
	use nuclio\plugin\config\ConfigCollection;
	use \Swift_SmtpTransport;
	use \Swift_Mailer;
	use \Swift_Message;
	
	<<factory>>
	class Email extends Plugin
	{
		const DEFAULT_CONTENT_TYPE='text/html';
		
		//Config Get From App.neon
		private array $From=array();

		private bool $UseSmtp=false;

		private bool $UseSSL=false;

		private int $port;

		private string $host;

		private string $username;

		private string $password;
		//End Config
		
		private mixed $transport;

		private mixed $mailer;
		
		private ConfigCollection $config;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Email
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		/*
		This is auto Construct file load config and assign variable.
		 */
		public function __construct(ConfigCollection $config)
		{
			$this->config=$config;
			$confFrom=$this->config->get('From');
			if(is_array($confFrom) && !is_null($confFrom['Email']))
			{
				$this->From =[$confFrom['Email']=>$confFrom['Name']];
			}
			$this->UseSmtp 	=(bool)$this->config->get('UseSmtp');
			$this->UseSSL	=(bool)$this->config->get('UseSSL');
			$this->port		=(int)$this->config->get('Port');
			$this->host		=(string)$this->config->get('Host');
			$this->username	=(string)$this->config->get('username');
			$this->password	=(string)$this->config->get('password');
			//Load Smtp Transport
			$this->transport=$this->SwiftSmtpTransport();
			//Load Mailer
			$this->mailer 	=$this->SwiftMailer($this->transport);
			parent::__construct();
		}

		/**
		 * Send Email Function Use for Sending email Use Library for Swiftmailer.
		 * @param  array  $to      Define to email address in array, single or multiple
		 * @param  string $body    Define Body in String or HTML format.
		 * @param  string $subject Define Subject in String Only
		 * @return Int             Return in int total number of email sent.
		 */
		public function sendEmail(array $to, string $body, string $subject):int
		{
			// Create the Mailer using your created Transport
			return $this->SwiftMessage($to,$subject,$body);
		}

		private function SwiftMailer(mixed $transport):mixed
		{
			return Swift_Mailer::newInstance($transport);
		}


		private function SwiftSmtpTransport():mixed
		{
			if($this->UseSmtp)
			{
				return Swift_SmtpTransport::newInstance($this->host, $this->port, ($this->UseSSL==true?'ssl':''))
			   ->setUsername($this->username)
			   ->setPassword($this->password);
			}
			else
			{
				return Swift_SmtpTransport::newInstance();
			}
		}

		private function SwiftMessage(array $to, string $subject, string $body, string $contentType=self::DEFAULT_CONTENT_TYPE):int
		{
			// Create a message
			$numSent = 0;
			 $message = Swift_Message::newInstance($subject)
			 ->setFrom($this->From)
			 ->setTo($to)
			 ->setBody($body)
			 ->setContentType($contentType);
			// Send the message
			if($this->mailer instanceof Swift_Mailer)
			{
				$numSent = $this->mailer->send($message);
			}
			return $numSent;
		}
	}
}
